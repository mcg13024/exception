package week03;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main{

    public static int divided (int num1, int num2)
            throws ArithmeticException
    {
        return  num1 / num2;

    }

    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        boolean continueLoop = true;
        do {
            try
            {
                System.out.println("Please enter a number you want to divide: ");
                int num1 = scanner.nextInt();
                System.out.println("Please enter the number you want to divided by: ");
                int num2 = scanner.nextInt();
                int result = divided(num1, num2);
                System.out.println("num1: " + num1);
                System.out.println("num2: " + num2);
                System.out.println("Result: " + result );
                continueLoop = false;
            }catch (InputMismatchException inputMismatchException)
            {
                System.err.println("Exeption: " + inputMismatchException);
                scanner.nextLine();
                System.out.println("You must enter whole numbers. Please try again.\n");
                continueLoop = true;
            }catch (ArithmeticException arithmeticExpeption)
            {
                System.err.println("Exception: " + arithmeticExpeption);
                System.out.println("Unable to divide by zero. Please try again. \n\n");;
                continueLoop = true;
            }
        }while (continueLoop);
    }
}